const pkg = require('./package');

module.exports = {
  resolve: {
    alias: {
      '@main': 'src/',
      '@assets': 'src/assets/',
    },
  },
  module: {
    // rules: [
    //     {
    //         test: /\.(png|svg|jpg|jpeg|gif)$/i,
    //         type: 'asset/resource',
    //     },
    // ]
  },
  output: {
    publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`,
  },
};
