module.exports = {
	apiPath: 'stubs/api',
	webpackConfig: require('./webpack.config'),
	navigations: {
		'publicium.nav.home': '/',
		'publicium.nav.collection': '/user/:id/collection',
		'publicium.nav.user': '/user/:id',
		'publicium.nav.games': '/games',
		'publicium.nav.signup': '/signup',
		'publicium.nav.signin': '/signin',
		'publicium.nav.root': '/publicium'
	},
	config: {
		'publicium.apiRoot': '/multystub/publicium/'
	}
};
