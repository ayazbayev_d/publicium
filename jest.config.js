module.exports = {
	clearMocks: true,
	coverageDirectory: 'reports/coverage',
	coverageProvider: 'babel',
	coverageReporters: [
		[
			'html',
			{
				subdir: 'html',
			},
		],
	],
	moduleNameMapper: {
		'\\.(css)$': 'identity-obj-proxy',
		'\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
			'<rootDir>/__mocks__/fileMock.js',
		'@ijl/cli': '<rootDir>/__mocks__/ijl.cli.js',
		'^@assets(.*)$': '<rootDir>/src/assets$1',
		'^@main(.*)$': '<rootDir>/src$1',
		// "react-hook-form": "<rootDir>/__mocks__/react-hook-form.js",
		// "react-router-dom": "<rootDir>/__mocks__/react-router-dom.js",
		// 'i18next': '<rootDir>/__mocks__/i18next.js',
	},
	preset: 'ts-jest',
	roots: ['<rootDir>'],
	setupFilesAfterEnv: ['./setup-test.js'],
	snapshotSerializers: ['enzyme-to-json/serializer'],
	testEnvironment: 'enzyme',
	testEnvironmentOptions: {
		enzymeAdapter: 'react16',
	},
	testMatch: [
		'**/__tests__/**/?(*.)+(spec|test).[tj]s?(x)',
	],
};
