import apex from './apex.png';
import bms from './bms.png';
import cod from './cod.png';
import cs16 from './cs16.png';
import csgo from './csgo.png';
import fallout from './fallout.png';
import hl from './hl.png';
import l4d from './l4d.png';
import mc from './mc.png';
import pubg from './pubg.png';
import stalker from './stalker.png';
import tes5 from './tes5.png';
import wot from './wot.png';

interface AnyLogoMap {
	[key: string]: string | undefined;
}

const GameLogosInternal = {
	apex,
	bms,
	cod,
	cs16,
	csgo,
	fallout,
	hl,
	l4d,
	mc,
	pubg,
	stalker,
	tes5,
	wot,
};

type LogoMap = typeof GameLogosInternal & AnyLogoMap;

const GameLogos: LogoMap = GameLogosInternal;

export default GameLogos;
