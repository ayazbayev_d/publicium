export { default as HomeIcon } from './home.png';
export { default as JoyIcon } from './joystick.png';
export { default as AppsIcon } from './apps.png';
export { default as SearchIcon } from './search.png';
export { default as GitIcon } from './GitHub.png';
