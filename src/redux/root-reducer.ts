import { HomePageReducer } from '@main/pages/home/redux';

const rootReducer = {
	home: HomePageReducer,
};

export default rootReducer;
