import React, { FC } from 'react';

import {
	Section,
	SectionTitle,
	StatContents,
	StatEntry,
	StatSection,
	MapName,
	MapGame,
} from './blocks';

const Sections: FC = () => (
	<Section>
		<StatSection>
			<SectionTitle>CS:GO</SectionTitle>
			<StatContents>
				<StatEntry>
					{/* <StatIcon src={Emblem} alt={'Person'} /> */}
					<MapName>Lost heaven</MapName>
					<MapGame>46mb</MapGame>
				</StatEntry>
				<StatEntry>
					{/* <StatIcon src={Emblem} alt={'Person'} /> */}
					<MapName>Kinder Garden</MapName>
					<MapGame>38mb</MapGame>
				</StatEntry>
				<StatEntry>
					{/* <StatIcon src={Emblem} alt={'Person'} /> */}
					<MapName>Parking</MapName>
					<MapGame>41mb</MapGame>
				</StatEntry>
				<StatEntry>
					{/* <StatIcon src={Emblem} alt={'Person'} /> */}
					<MapName>Zima Blue</MapName>
					<MapGame>23mb</MapGame>
				</StatEntry>
			</StatContents>
		</StatSection>

		<StatSection>
			<SectionTitle>Left 4 Dead</SectionTitle>
			<StatContents>
				<StatEntry>
					{/* <StatIcon src={Emblem} alt={'Person'} /> */}
					<MapName>Mall</MapName>
					<MapGame>31mb</MapGame>
				</StatEntry>
				<StatEntry>
					{/* <StatIcon src={Emblem} alt={'Person'} /> */}
					<MapName>Mega</MapName>
					<MapGame>65mb</MapGame>
				</StatEntry>
				<StatEntry>
					{/* <StatIcon src={Emblem} alt={'Person'} /> */}
					<MapName>Titan army</MapName>
					<MapGame>34mb</MapGame>
				</StatEntry>
				<StatEntry>
					{/* <StatIcon src={Emblem} alt={'Person'} /> */}
					<MapName>Cinema alley</MapName>
					<MapGame>13mb</MapGame>
				</StatEntry>
			</StatContents>
		</StatSection>

		<StatSection>
			<SectionTitle>Minecraft</SectionTitle>
			<StatContents>
				<StatEntry>
					{/* <StatIcon src={Emblem} alt={'Person'} /> */}
					<MapName>Parkour arena</MapName>
					<MapGame>27mb</MapGame>
				</StatEntry>
				<StatEntry>
					{/* <StatIcon src={Emblem} alt={'Person'} /> */}
					<MapName>Cripper crusade</MapName>
					<MapGame>31mb</MapGame>
				</StatEntry>
				<StatEntry>
					{/* <StatIcon src={Emblem} alt={'Person'} /> */}
					<MapName>Skeleton crusade</MapName>
					<MapGame>76mb</MapGame>
				</StatEntry>
				<StatEntry>
					{/* <StatIcon src={Emblem} alt={'Person'} /> */}
					<MapName>Ender Dragon crusade</MapName>
					<MapGame>63mb</MapGame>
				</StatEntry>
			</StatContents>
		</StatSection>

	</Section>
);

export default Sections;
