import styled from 'styled-components';

export const Section = styled.section`
  overflow-y: scroll;
  display: flex;
  flex-direction: column;
  margin: 0.75rem 2.5rem;
`;

export const SectionTitle = styled.h2`
	margin-left: 0.5rem;
`;

export const Horizon = styled.div`	
  overflow-x: auto;
  overflow-y: visible;
  
  scroll-behavior: smooth;

  padding-bottom: 0.5rem;

  white-space: nowrap;
  

  //&::-webkit-scrollbar {
  //  width: 0;
  //}

  //// noinspection CssUnknownProperty
  //scrollbar-width: none; /* For firefox */
  //-ms-overflow-style: none;
`;

export const TopGame = styled.div<{logo: string}>`
  display: inline-block;
  
  background-color: #ececec;
  box-shadow: 0 4px 4px 1px rgba(0, 0, 0, 0.25);
  
  background-image: url('${({ logo }) => logo}');
  background-size: 40%;
  background-position: center;
  background-repeat: no-repeat;

  width: 18.75rem;
  height: 8.75rem;

  margin: 0 1.5rem;
  
`;

export const HorizontalSections = styled.div`
  display: flex;

  flex-direction: row;

  align-items: flex-start;
  justify-content: stretch;
  
  margin: 0.75rem 2.5rem;
`;

export const StatSection = styled.div`

  flex-grow: 1;

  margin: 0 0.75rem;

  padding: 0.3rem;

  margin-top: 1.5rem;

  background-color: white;

  border-radius: 0.5rem;

  box-shadow: 0 4px 4px 1px rgba(0, 0, 0, 0.25);
`;


export const StatContents = styled.div`
  display: flex;
  flex-direction: column;

  padding: 0 1rem 1rem;

  align-items: stretch;
  justify-content: flex-start;
`;

export const StatEntry = styled.div`
  display: flex;
  
  flex-direction: row;
  
  align-items: center;
  
  background-color: #ececec;
  
  margin: 0.5rem 0;
  
  padding: 0 1rem;
  
  border-radius: 0.5rem;
`;

export const StatIcon = styled.img`
  border-radius: 50%;
  
  height: 2.5rem;
  width: auto;
  
  margin: 0.5rem;
`;

export const UploaderName = styled.div`
  font-size: 1.25rem;
  font-weight: 500;
`;

export const MapName = styled.div`
  font-size: 1.25rem;
  
  flex-grow: 1;
`;

export const MapGame = styled.div`
  font-size: 1.25rem;
  font-weight: bold;
  
  //flex-grow: 1;
`;
