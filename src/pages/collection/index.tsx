import Search from '@main/common/search';
import { NavSection } from '@main/common/sidebar/nav';
import Sections from '@main/pages/collection/sections';
import React, { FC } from 'react';
import {
	Sidebar,
	Viewport,
	Content,
} from '@main/common';


const Collection: FC = () => (
	<Viewport>
		<Sidebar navSection={NavSection.Collection} />
		<Content>
			<Search />
			{/* <Promo /> */}
			<Sections />
		</Content>
	</Viewport>
);

export default Collection;
