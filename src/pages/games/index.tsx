import Search from '@main/common/search';
import { NavSection } from '@main/common/sidebar/nav';
import React, { FC } from 'react';
import {
	Sidebar,
	Viewport,
	Content,
} from '@main/common';
import ListOfGames from './sections';


const Games: FC = () => (
	<Viewport>
		<Sidebar navSection={NavSection.Games} />
		<Content>
			<Search />
			<ListOfGames />
		</Content>
	</Viewport>
);

export default Games;
