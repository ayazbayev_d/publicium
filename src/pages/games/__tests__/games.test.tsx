import Theme from '@main/config/theme';
import {
  BrowserRouter as Router,
} from 'react-router-dom';

import { mount } from 'enzyme';
import React from 'react';
import { describe, it, expect } from '@jest/globals';
import { ThemeProvider } from 'styled-components';
import Games from '../index';

describe('<Games />', () => {
  it('Games renders OK', () => {
    const component = mount(
      <ThemeProvider theme={Theme}>
        <Router>
          <Games />
        </Router>
      </ThemeProvider>,
    );

    expect(component).toMatchSnapshot();
  });
});

