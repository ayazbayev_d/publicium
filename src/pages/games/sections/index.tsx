import React, { FC } from 'react';
import 'react-simple-flex-grid/lib/main.css';
import Gallery from 'react-grid-gallery';
import GamesList from '../games-list';

const ListOfGames: FC = () => (
	<Gallery enableImageSelection={false} images={GamesList} />
);

export default ListOfGames;
