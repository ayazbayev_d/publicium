import GameLogos from '@main/assets/images/games';

export default [
	{
		'src': GameLogos.bms,
		'thumbnail': GameLogos.bms,
		'caption': 'BlackMesa'
	},
	{
		'src': GameLogos.pubg,
		'thumbnail': GameLogos.pubg,
  
		'caption': 'PUBG'
	},
	{
		'src': GameLogos.csgo,
		'thumbnail': GameLogos.csgo,
		'caption': 'CS_GO'
	},

	{
		'src': GameLogos.apex,
		'thumbnail': GameLogos.apex,
		'caption': 'ApexLegends'
	},
	{
		'src': GameLogos.wot,
		'thumbnail': GameLogos.wot,
  
		'caption': 'WoT'
	},
	{
		'src': GameLogos.stalker,
		'thumbnail': GameLogos.stalker,
		'caption': 'STALKER'
	},

	{
		'src': GameLogos.mc,
		'thumbnail': GameLogos.mc,
		'caption': 'MINECRAFT'
	},
	{
		'src': GameLogos.cod,
		'thumbnail': GameLogos.cod,
  
		'caption': 'CoD'
	},
	{
		'src': GameLogos.cs16,
		'thumbnail': GameLogos.cs16,
		'caption': 'CS1.6'
	},

	{
		'src': GameLogos.l4d,
		'thumbnail': GameLogos.l4d,
		'caption': 'L4D'
	},
	{
		'src': GameLogos.fallout,
		'thumbnail': GameLogos.fallout,
  
		'caption': 'Fallout'
	},
	{
		'src': GameLogos.tes5,
		'thumbnail': GameLogos.tes5,
		'caption': 'TES'
	}
];