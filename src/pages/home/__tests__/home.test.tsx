import Theme from '@main/config/theme';
import {
  BrowserRouter as Router,
} from 'react-router-dom';

import { mount } from 'enzyme';
import React from 'react';
import { describe, it, expect } from '@jest/globals';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import store from '@main/redux/store';
import Home from '../index';


describe('<Home/>', () => {
  it('Home renders OK', () => {
    const component = mount(
      <ThemeProvider theme={Theme}>
        <Provider store={store}>
          <Router>
            <Home />
          </Router>
        </Provider>
      </ThemeProvider>,
    );
    expect(component).toMatchSnapshot();
  });
});

