import {
	FetchableList,
	Game,
	GameMap,
	User,
} from '@main/common/types';
import HOME_ACTIONS from '@main/pages/home/sections/actions';
import { createSlice } from '@reduxjs/toolkit';
import { compose } from 'redux';

export interface HomePageState {
	topGames: FetchableList<Game>;
	topMaps: FetchableList<GameMap>;
	topUsers: FetchableList<User>;
}

const initialState: HomePageState = {
	topGames: {
		loading: false,
		items: [],
	},
	topMaps: {
		loading: false,
		items: [],
	},
	topUsers: {
		loading: false,
		items: [],
	},
};

const { reducer, actions } = createSlice({
	name: 'home',
	initialState,
	extraReducers: compose(
		HOME_ACTIONS.extras,
	),
	reducers: {
		...HOME_ACTIONS.actions,
	},
});

export const HomePageActions = {
	...actions,
	...HOME_ACTIONS.thunks,
};
export const HomePageReducer = reducer;
