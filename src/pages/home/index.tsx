import Search from '@main/common/search';
import { NavSection } from '@main/common/sidebar/nav';
import Sections from '@main/pages/home/sections';
import React, { FC } from 'react';
import {
	Sidebar,
	Viewport,
	Content,
} from '@main/common';
import { Promo } from './blocks';


const Home: FC = () => (
	<Viewport>
		<Sidebar navSection={NavSection.Home} />
		<Content>
			<Search />
			<Promo />
			<Sections />
		</Content>
	</Viewport>
);

export default Home;
