import Emblem from '@assets/images/emblem.png';
import {
	FetchableList,
	GameMap,
} from '@main/common/types';
import { HomePageActions } from '@main/pages/home/redux';
import {
	MapGame,
	MapName,
	SectionTitle,
	StatContents,
	StatEntry,
	StatIcon,
	StatSection,
} from '@main/pages/home/sections/blocks';
import {
	AppDispatch,
	RootState,
} from '@main/redux/store';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

type TopMapsProps = FetchableList<GameMap> & {
	dispatch: AppDispatch;
};

class TopMaps extends Component<TopMapsProps> {
	componentDidMount() {
		const { dispatch } = this.props;

		void dispatch(HomePageActions.fetchTopMaps());
	}

	render() {
		const { error, loading, items } = this.props;

		return (
			<StatSection>
				<SectionTitle>Top Maps</SectionTitle>
				<StatContents>
					{error && (
						<StatEntry>
							<MapName>{error}</MapName>
						</StatEntry>
					)}
					{loading && (
						<StatEntry>
							<MapName>Loading...</MapName>
						</StatEntry>
					)}
					{items.map(item => (
						<StatEntry>
							<StatIcon src={Emblem} alt={'Person'} />
							<MapName>{item.name}</MapName>
							<MapGame>{item.game.name}</MapGame>
						</StatEntry>
					))}
				</StatContents>
			</StatSection>
		);
	}
}

export default compose(
	connect(
		({ home: { topMaps } }: RootState) => topMaps,
		dispatch => ({ dispatch }),
	),
)(TopMaps);
