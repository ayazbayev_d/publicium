import Person from '@assets/images/person.png';
import {
	FetchableList,
	User,
} from '@main/common/types';
import { HomePageActions } from '@main/pages/home/redux';
import {
	SectionTitle,
	StatContents,
	StatEntry,
	StatIcon,
	StatSection,
	UploaderName,
} from '@main/pages/home/sections/blocks';
import {
	AppDispatch,
	RootState,
} from '@main/redux/store';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

type TopUploadersProps = FetchableList<User> & {
	dispatch: AppDispatch;
}

class TopUploaders extends Component<TopUploadersProps> {
	componentDidMount() {
		const { dispatch } = this.props;

		void dispatch(HomePageActions.fetchTopUsers());
	}


	render() {
		const { loading, error, items } = this.props;

		return (
			<StatSection>
				<SectionTitle>Top Uploaders</SectionTitle>
				<StatContents>
					{loading && (
						<StatEntry>
							<UploaderName>Loading...</UploaderName>
						</StatEntry>
					)}
					{error && (
						<StatEntry>
							<UploaderName>Something went wrong</UploaderName>
						</StatEntry>
					)}
					{items.map(item => (
						<StatEntry>
							<StatIcon src={Person} />
							<UploaderName>{item.name}</UploaderName>
						</StatEntry>
					))}
				</StatContents>
			</StatSection>
		);
	}
}

export default compose(
	connect(
		({ home: { topUsers } }: RootState) => topUsers,
		dispatch => ({ dispatch }),
	),
)(TopUploaders);
