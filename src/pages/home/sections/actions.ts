import HomeApi from '@main/api/home-api';
import { HomePageState } from '@main/pages/home/redux';
import {
	ActionReducerMapBuilder,
	createAsyncThunk,
} from '@reduxjs/toolkit';

const fetchTopGames = createAsyncThunk(
	'home/fetchTopGames',
	() => HomeApi.fetchTrendingGames(),
);

const fetchTopUsers = createAsyncThunk(
	'home/fetchTopUsers',
	() => HomeApi.fetchTopUsers(),
);

const fetchTopMaps = createAsyncThunk(
	'home/fetchTopMaps',
	() => HomeApi.fetchTopMaps(),
);

const extras = (builder: ActionReducerMapBuilder<HomePageState>): ActionReducerMapBuilder<HomePageState> => (
	builder
	// region TopGames
		.addCase(fetchTopGames.pending, state => ({
			...state,
			topGames: {
				loading: true,
				error: undefined,
				items: [],
			},
		}))
		.addCase(fetchTopGames.fulfilled, (
			state,
			{ payload },
		) => ({
			...state,
			topGames: {
				loading: false,
				error: undefined,
				items: payload,
			},
		}))
		.addCase(fetchTopGames.rejected, (
			state,
			{ error },
		) => ({
			...state,
			topGames: {
				loading: false,
				error: error.message,
				items: [],
			},
		}))
	// endregion
	// region TopUsers
		.addCase(fetchTopUsers.pending, state => ({
			...state,
			topUsers: {
				loading: true,
				error: undefined,
				items: [],
			},
		}))
		.addCase(fetchTopUsers.fulfilled, (
			state,
			{ payload },
		) => ({
			...state,
			topUsers: {
				loading: false,
				error: undefined,
				items: payload,
			},
		}))
		.addCase(fetchTopUsers.rejected, (
			state,
			{ error },
		) => ({
			...state,
			topUsers: {
				loading: false,
				error: error.message,
				items: [],
			},
		}))
	// endregion
	// region TopMaps
		.addCase(fetchTopMaps.pending, state => ({
			...state,
			topMaps: {
				loading: true,
				error: undefined,
				items: [],
			},
		}))
		.addCase(fetchTopMaps.fulfilled, (
			state,
			{ payload },
		) => ({
			...state,
			topMaps: {
				loading: false,
				error: undefined,
				items: payload,
			},
		}))
		.addCase(fetchTopMaps.rejected, (
			state,
			{ error },
		) => ({
			...state,
			topMaps: {
				loading: false,
				error: error.message,
				items: [],
			},
		}))
	// endregion
);

const HOME_ACTIONS = {
	actions: {},
	thunks: {
		fetchTopGames,
		fetchTopUsers,
		fetchTopMaps,
	},
	extras,
};

export default HOME_ACTIONS;
