import React from 'react';
import Theme from '@main/config/theme';
import {
  BrowserRouter as Router,
} from 'react-router-dom';

import { mount } from 'enzyme';
import { describe, it, expect, beforeEach, beforeAll, afterAll } from '@jest/globals';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import store from '@main/redux/store';
import {
  apiUrl,
} from '@main/api/util';
import TopUploaders from '../top-uploaders';
import { awaitTimeout, createAxiosMock } from './axios';


const sampleData = [
  {
    id: 1,
    name: 'Foo Bar',
    avatar: 'user.png',
  },
  {
    id: 31,
    name: 'Holy Radar',
    avatar: 'user.png',
  },
  {
    id: 12,
    name: 'Gordon Freeman',
    avatar: 'user.png',
  },
  {
    id: 43,
    name: 'Portator Defunctorum',
    avatar: 'user.png',
  },
];

describe('<TopUploaders /> with no api errors', () => {
  let mockAxios = createAxiosMock();

  beforeAll(() => {
    mockAxios = createAxiosMock();

    mockAxios.onGet(apiUrl('/users/top-load')).reply(200, sampleData);
  });

  afterAll(() => {
    mockAxios.restore();
  });

  it('Renders in loading state', () => {
    const component = mount(
      <ThemeProvider theme={Theme}>
        <Provider store={store}>
          <Router>
            <TopUploaders />
          </Router>
        </Provider>
      </ThemeProvider>,
    );

    expect(component).toMatchSnapshot();
  });

  it('Displays data with no errors', async () => {
    mockAxios.onGet(apiUrl('/users/top-load')).replyOnce(200, sampleData);

    const component = mount(
      <ThemeProvider theme={Theme}>
        <Provider store={store}>
          <Router>
            <TopUploaders />
          </Router>
        </Provider>
      </ThemeProvider>,
    );


    await awaitTimeout(1000)
      .then(() => component.update())
      .then(
        () => expect(component).toMatchSnapshot(),
      );
  });
});
describe('<TopUploaders /> with no api errors', () => {
  let mockAxios = createAxiosMock();

  beforeAll(() => {
    mockAxios = createAxiosMock();
    
    mockAxios.onGet(apiUrl('/users/top-load')).networkError();
  });

  afterAll(() => {
    mockAxios.restore();
  });

  it('Displays errors', async () => {
    const component = mount(
      <ThemeProvider theme={Theme}>
        <Provider store={store}>
          <Router>
            <TopUploaders />
          </Router>
        </Provider>
      </ThemeProvider>,
    );


    await awaitTimeout(1000)
      .then(() => component.update())
      .then(
        () => expect(component).toMatchSnapshot(),
      );
  });
});