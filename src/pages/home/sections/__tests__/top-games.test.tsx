import React from 'react';
import Theme from '@main/config/theme';
import {
  BrowserRouter as Router,
} from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { mount } from 'enzyme';
import { describe, it, expect, beforeEach, beforeAll, afterAll } from '@jest/globals';
import { Provider } from 'react-redux';
import store from '@main/redux/store';
import {
  apiUrl,
} from '@main/api/util';
import { createAxiosMock, awaitTimeout } from './axios';
import TopGames from '../top-games';


const sampleData = [
  {
    id: 1,
    name: 'Black Mesa',
    logo: 'bms',
  },
  {
    id: 12,
    name: 'Minecraft',
    logo: 'mc',
  },
  {
    id: 31,
    name: 'CS: GO',
    logo: 'csgo',
  },
  {
    id: 43,
    name: 'Left 4 Dead',
    logo: 'l4d',
  },
];

describe('<TopGames /> with no api errors', () => {
  let mockAxios = createAxiosMock();

  beforeAll(() => {
    mockAxios = createAxiosMock();

    mockAxios.onGet(apiUrl('/games/trending')).reply(200, sampleData);
  });

  afterAll(() => {
    mockAxios.restore();
  })

  it('Renders in loading state', () => {
    const component = mount(
      <ThemeProvider theme={Theme}>
        <Provider store={store}>
          <Router>
            <TopGames />
          </Router>
        </Provider>
      </ThemeProvider>,
    );

    expect(component).toMatchSnapshot();
  });

  it('Displays data with no errors', async () => {
    const component = mount(
      <ThemeProvider theme={Theme}>
        <Provider store={store}>
          <Router>
            <TopGames />
          </Router>
        </Provider>
      </ThemeProvider>,
    );


    await awaitTimeout(1000)
      .then(() => component.update())
      .then(
        () => expect(component).toMatchSnapshot(),
      );
  });
});
describe('<TopGames /> with api errors', () => {
  let mockAxios = createAxiosMock();

  beforeAll(() => {
    mockAxios = createAxiosMock();

    mockAxios.onGet(apiUrl('/games/trending')).networkError();
  });

  afterAll(() => {
    mockAxios.restore();
  });

  it('Displays errors', async () => {
    const component = mount(
      <ThemeProvider theme={Theme}>
        <Provider store={store}>
          <Router>
            <TopGames />
          </Router>
        </Provider>
      </ThemeProvider>,
    );
  
  
    await awaitTimeout(1000)
      .then(() => component.update())
      .then(
        () => expect(component).toMatchSnapshot(),
      );
  });
  
});