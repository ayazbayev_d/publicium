import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

export const awaitTimeout = (delay: number): Promise<void> => new Promise(resolve => {
	setTimeout(resolve, delay);
});
  
export const createAxiosMock = () =>  new MockAdapter(axios, { delayResponse: 50 });
