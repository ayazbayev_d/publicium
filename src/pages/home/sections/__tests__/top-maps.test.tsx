import React from 'react';
import { afterAll, afterEach, beforeAll, beforeEach, describe, expect, it } from '@jest/globals';
import {
  apiUrl, clapf
} from '@main/api/util';
import Theme from '@main/config/theme';
import { ThemeProvider } from 'styled-components';
import store from '@main/redux/store';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router
} from 'react-router-dom';
import { createAxiosMock, awaitTimeout } from './axios';
import TopMaps from '../top-maps';
import MockAdapter from 'axios-mock-adapter/types';


const sampleData = [
  {
    id: 302,
    name: 'Lambda Core 1',
    logo: 'lambda-1.png',
    game: 1,
  },
  {
    id: 12,
    name: 'Inferno',
    logo: 'csgo-inferno.png',
    game: 31,
  },
  {
    id: 43,
    name: 'Ice Spikes',
    logo: 'mc-icespk.png',
    game: 12,
  },
  {
    id: 9,
    name: 'Dust 2',
    logo: 'csgo-dust2.png',
    game: 31,
  },
];

const sampleGamesData = [
  {
    id: 1,
    name: 'Black Mesa',
    logo: 'bms',
  },
  {
    id: 12,
    name: 'Minecraft',
    logo: 'mc',
  },
  {
    id: 31,
    name: 'CS: GO',
    logo: 'csgo',
  },
];

describe('<TopMaps /> with no api errors', () => {
  let mockAxios = createAxiosMock();
  
  beforeAll(() => {
    mockAxios = createAxiosMock();

    mockAxios.onGet(apiUrl('/maps/trending')).reply(200, sampleData);

    sampleGamesData.forEach(game => (
      mockAxios.onGet(
        clapf(apiUrl(`/games/${game.id}`), 'Registered mock on: "%s"')
      ).reply(200, game)
    ));
  })

  afterAll(() => {
    mockAxios.restore();
  });

  it('Renders in loading state', () => {
    const component = mount(
      <ThemeProvider theme={Theme}>
        <Provider store={store}>
          <Router>
            <TopMaps />
          </Router>
        </Provider>
      </ThemeProvider>,
    );

    expect(component).toMatchSnapshot();
  });


  it('Displays data with no errors', async () => {
    const component = mount(
      <ThemeProvider theme={Theme}>
        <Provider store={store}>
          <Router>
            <TopMaps />
          </Router>
        </Provider>
      </ThemeProvider>,
    );


    await awaitTimeout(2400)
      .then(() => component.update())
      .then(
        () => expect(component).toMatchSnapshot(),
      );
  });
});


describe('<TopMaps /> with api errors', () => {
  let mockAxios = createAxiosMock();
  
  beforeAll(() => {
    mockAxios = createAxiosMock();

    mockAxios.onGet(apiUrl('/maps/trending')).networkError();

    sampleGamesData.forEach(game => (
      mockAxios.onGet(
        clapf(apiUrl(`/games/${game.id}`), 'Registered mock on: "%s"')
      ).networkError()
    ));
  })

  afterAll(() => {
    mockAxios.restore();
  });

  it('Displays errors', async () => {
    mockAxios.onGet(apiUrl('/maps/trending')).networkErrorOnce();

    const component = mount(
      <ThemeProvider theme={Theme}>
        <Provider store={store}>
          <Router>
            <TopMaps />
          </Router>
        </Provider>
      </ThemeProvider>,
    );


    await awaitTimeout(200)
      .then(() => component.update())
      .then(
        () => expect(component).toMatchSnapshot(),
      );
  });
});