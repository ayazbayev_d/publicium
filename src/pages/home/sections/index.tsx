import TopGames from '@main/pages/home/sections/top-games';
import React, { FC } from 'react';
import TopMaps from '@main/pages/home/sections/top-maps';
import TopUploaders from '@main/pages/home/sections/top-uploaders';
import { HorizontalSections } from './blocks';

const Sections: FC = () => (
	<>
		<TopGames />
		<HorizontalSections>
			<TopUploaders />
			<TopMaps />
		</HorizontalSections>
	</>
);

export default Sections;
