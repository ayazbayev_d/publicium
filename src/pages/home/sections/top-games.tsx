import {
	FetchableList,
	Game,
} from '@main/common/types';
import { HomePageActions } from '@main/pages/home/redux';
import {
	Horizon,
	Section,
	SectionTitle,
	TopGame,
} from '@main/pages/home/sections/blocks';
import {
	AppDispatch,
	RootState,
} from '@main/redux/store';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import GameLogos from '@main/assets/images/games';

type TopGamesProps = FetchableList<Game> & {
	dispatch: AppDispatch;
};

class TopGames extends Component<TopGamesProps> {
	componentDidMount() {
		const { dispatch } = this.props;

		void dispatch(HomePageActions.fetchTopGames());
	}

	render() {
		const { loading, items } = this.props;

		return (
			<Section>
				<SectionTitle>
					Top Games
				</SectionTitle>
				<Horizon>
					{loading && 'Loading...'}
					{items.map(({ logo }) => (
						<TopGame logo={GameLogos[logo] ?? ''} />
					))}
				</Horizon>
			</Section>
		);
	}
}

export default compose(
	connect(({ home: { topGames } }: RootState) => topGames),
)(TopGames);
