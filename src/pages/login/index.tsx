import { NavSection } from '@main/common/sidebar/nav';
import React, { FC } from 'react';
import {
	Sidebar,
	Viewport,
} from '@main/common';
import {
	Container,
	Empty,
	LoginButton,
	ViewportCol,
	ViewportRow,
	UsernameInput,
	PasswordInput,
	AuthMode,
	SignupButton,
	LoginText,
	SignupText,
	ForgotPasswordButton,
	ForgotPasswordBlock,
	ForgotPasswordText,
	AuthorizeBlock,
	AuthorizeButton,
	AuthorizeText,
	ConnectWithBlock,
	ConnectWithGit,
	InputContainer,
	OrLine,
} from './blocks';

const Login: FC = () => (
	<Viewport>
		<Sidebar navSection={NavSection.Service} />
		<ViewportRow>
			<Empty />
			<ViewportCol>
				<Empty />
				<Container>
					<AuthMode>
						<LoginButton>
							<LoginText>Log In</LoginText>
						</LoginButton>
						<SignupButton>
							<SignupText>Sign Up</SignupText>
						</SignupButton>
					</AuthMode>
					<InputContainer>
						<Empty />
						<UsernameInput placeholder={'Username'} />
						<Empty />
					</InputContainer>
					<InputContainer>
						<Empty />
						<PasswordInput placeholder={'Password'} />
						<Empty />
					</InputContainer>


					<AuthorizeBlock>
						<AuthorizeButton>
							<AuthorizeText>
								Log In
							</AuthorizeText>
						</AuthorizeButton>
					</AuthorizeBlock>

					<ForgotPasswordBlock>
						<ForgotPasswordButton>
							<ForgotPasswordText>
								Forgot Password?
							</ForgotPasswordText>
						</ForgotPasswordButton>
					</ForgotPasswordBlock>

					<OrLine text={'Or connect with'} />

					<ConnectWithBlock>
						<ConnectWithGit />
						<ConnectWithGit />
						<ConnectWithGit />
						<ConnectWithGit />
					</ConnectWithBlock>
				</Container>
				<Empty />
			</ViewportCol>
			<Empty />
		</ViewportRow>
	</Viewport>
);

export default Login;
