import { NavSection } from '@main/common/sidebar/nav';
import Sections from '@main/pages/profile/sections';
import React, { FC } from 'react';
import {
	Sidebar,
	Viewport,
	Content,
} from '@main/common';


const Profile: FC = () => (
	<Viewport>
		<Sidebar navSection={NavSection.Collection} />
		<Content>
			{/* <Search /> */}
			{/* <Promo /> */}
			<Sections />
		</Content>
	</Viewport>
);

export default Profile;
