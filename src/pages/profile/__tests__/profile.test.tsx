import Theme from '@main/config/theme';
import {
  BrowserRouter as Router,
} from 'react-router-dom';

import { mount } from 'enzyme';
import React from 'react';
import { describe, it, expect } from '@jest/globals';
import { ThemeProvider } from 'styled-components';
import Profile from '../index';

describe('<Profile />', () => {
  it('Profile renders OK', () => {
    const component = mount(
      <ThemeProvider theme={Theme}>
        <Router>
          <Profile />
        </Router>
      </ThemeProvider>,
    );

    expect(component).toMatchSnapshot();
  });
});

