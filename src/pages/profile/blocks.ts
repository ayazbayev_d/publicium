import styled from 'styled-components';

export const Promo = styled.div`
  height: 10rem;
  
  background-color: ${({ theme }) => theme.colors.primary};
  
  background-image: url('https://dummyimage.com/800x240/031e44/ffffff&text=Promo+template');
  background-repeat: no-repeat;
  background-position: center;
  background-size: 20%;
  
  margin: 0.75rem 2.5rem;
  
  border-radius: 0.5rem;
`;
