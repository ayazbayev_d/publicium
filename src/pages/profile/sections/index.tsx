import React, { FC } from 'react';
import Person from '@assets/images/person.png';
import {
	Section,
	HatSection,
	ButtonSection,
	ProfileButton,
	UploadButton,
	EditButton,
	HatInfo,
	HatNick,
	HatAva,
	HatVertical,
} from './blocks';


const Sections: FC = () => (
	<>
		<HatSection>
			{/* <HatImage src={ProfileHat} alt={'Hat'} /> */}
			<HatVertical>
				<HatAva src={Person} alt={'Hat'} />
				<HatNick>Nickname</HatNick>
				<HatInfo>Interests</HatInfo>
				<HatInfo>Innopolis, Republick of Tatarstan, Russia</HatInfo>
			</HatVertical>

		</HatSection>

		<ButtonSection>
			<ProfileButton>About</ProfileButton>
			<ProfileButton>Downloads</ProfileButton>
			<ProfileButton>Likes</ProfileButton>
			<UploadButton>Upload map</UploadButton>
		</ButtonSection>

		<Section>

			<EditButton>Edit Profile</EditButton>

		</Section>
	</>
);

export default Sections;
