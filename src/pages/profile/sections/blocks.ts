import styled from 'styled-components';
import ProfileHat from '@assets/images/profile_hat.png';

export const Section = styled.section`
  overflow-y: scroll;
  align-items: center;
  display: flex;
  flex-direction: column;
  margin: 0.75rem 2.5rem;

  padding: 2rem;
`;

export const SectionTitle = styled.h2`
	margin-left: 0.5rem;
`;

export const HatSection = styled.section`
  margin: 0;
  width: 100%;
  padding: 1rem;

  background-image: url('${ProfileHat}');

  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
`;

export const HatVertical = styled.section`
  margin-top: 1rem;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const HatNick = styled.span`
  color: #ECECEC;
  font-size: 2.5rem;
`;

export const HatInfo = styled.span`
	color: #ECECEC;
  font-size: 1.125rem;
  // margin: 10px;
  padding: .5rem;
`;

export const HatImage = styled.img`
  width: 100%;
  height: 18.75rem;
  object-fit: cover;
  filter: saturate(120%);
`;

export const HatAva = styled.img`
  width: 7.5rem;
  height: 7.5rem;
  object-fit: cover;
  border-radius: 50%;
`;


export const ButtonSection = styled.section`
  width: 100%;
  height: 3.125rem;
  background-color: #293C56;

  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
`;

export const ProfileButton = styled.section`
  color: #ECECEC;
  text-align: center;

  width: 140px;
  
  line-height: 3.125rem;
  border-radius: 0px;

  transition: background-color 0.1s linear;
  &:hover {
    color: #9EED1D;
    background-color: #031E44;
    
  }
`;

export const UploadButton = styled.section`
  color: #ECECEC;
  text-align: center;

  background-color: #ED891D;
  border-radius: 0.625rem;
  width: 140px;
  line-height: 2.5rem;

  transition: background-color 0.1s linear;
  &:hover {
    color: black;
    background-color: #9EED1D;
    
  }
`;

export const EditButton = styled.section`
  color: #ECECEC;
  text-align: center;

  background-color: #031E44;
  border-radius: 0.625rem;
  width: 140px;
  line-height: 2.5rem;

  transition: background-color 0.1s linear;
  &:hover {
    // color: black;
    background-color: #ED891D;
    
  }
`;
