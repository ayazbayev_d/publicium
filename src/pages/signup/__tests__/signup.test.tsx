import Theme from '@main/config/theme';
import {
  BrowserRouter as Router,
} from 'react-router-dom';

import { mount } from 'enzyme';
import React from 'react';
import { describe, it, expect } from '@jest/globals';
import { ThemeProvider } from 'styled-components';
import SignUp from '../index';

describe('<SignUp />', () => {
  it('SignUp renders OK', () => {
    const component = mount(
      <ThemeProvider theme={Theme}>
        <Router>
          <SignUp />
        </Router>
      </ThemeProvider>,
    );

    expect(component).toMatchSnapshot();
  });
});

