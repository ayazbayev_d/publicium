import styled from 'styled-components';
import { GitIcon } from '@assets/icons';

export const ViewportRow = styled.div`
  
  display: flex;
  flex-direction: row;
  flex-grow: 1;
  
  align-items: stretch;
  justify-content: stretch;
`;

export const ViewportCol = styled.div`
  
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  
  align-items: stretch;
  justify-content: stretch;
`;

export const Empty = styled.section`
  display: flex;
  flex-direction: row;
  flex-grow: 1;
  background-color: #ABCDEF;
  align-items: center;
`;

export const Container = styled.section`
  display: flex;

  flex-direction: column;
  
  background-color: #ECEEF5;
  
  align-items: stretch;
  justify-content: stretch;
`;

export const AuthMode = styled.div`
  display: flex;
  flex-grow: 0.05;
  flex-direction: row;
  background-color: #ECEEF5;
  align-items: stretch;
  justify-content: stretch;
`;

export const LoginButton = styled.button` 
  outline: none;
  border: none;
  flex-grow: 1;
  flex-direction: row;
  align-items: center;
`;

export const LoginText = styled.h4`
  display: flex;
  flex-grow: 1;
  flex-direction: row;
  justify-content: center;
`;

export const SignupButton = styled.button`
  flex-grow: 1;
  flex-direction: row;
  align-items: center;
`;


export const SignupText = styled.h4`
  display: flex;
  flex-grow: 1;
  flex-direction: row;
  justify-content: center;
`;

export const ForgotPasswordBlock = styled.div`
  display: flex;
  flex-grow: 0.01;
  flex-direction: row;
  background-color: #ECEEF5;
  align-items: center;
  justify-content: center;
`;

export const ForgotPasswordButton = styled.button`
  border: none;
  flex-direction: row;
  background-color: #ECEEF5;
  align-items: center;
  justify-content: center;
`;

export const ForgotPasswordText = styled.h6`
    display: flex;
    flex-grow: 1;
    font-size: 0.69rem;
    flex-direction: row;
    justify-content: center;
`;

export const AuthorizeBlock = styled.div`
  display: flex;
  flex-direction: row;
  padding-top: 0.8rem;
  padding-bottom: 2rem;
  background-color: #ECEEF5;
  align-items: center;
  justify-content: center;
`;

export const AuthorizeButton = styled.button`
  flex-grow: 0.25;
  flex-direction: row;
  background-color: ${({ theme }) => theme.colors.primary};
  align-items: center;
  justify-content: center;
`;

export const AuthorizeText = styled.h4`
    display: flex;
    color: white;
    flex-grow: 1;
    flex-direction: row;
    justify-content: center;
`;


export const LineBlock = styled.div`
  display: flex;
  flex-grow: 0.05;
  flex-direction: row;
  justify-content: center;
`;

export const LineText = styled.h5`
  display: flex;
  flex-grow: 0.01;
  flex-direction: row;
  justify-content: center;
`;

export const ConnectWithBlock = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
  vertical-align: baseline;

  padding-top: 1.25rem;
  padding-bottom: 1.25rem;
`;

export const ConnectWithGit = styled.button`
  height: 2.5rem;
  width: 2.5rem;
  
  background-image: url('${GitIcon}');
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

  border: none;
  outline: none;
`;


export const InputContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

// export const InputFrame = styled.div`
//   display: flex;
//   flex-grow: 1;
//   flex-direction: row;
//   align-items: center;
// `;

export const UsernameInput = styled.input`

  outline: none;
  border: none;
  padding: 0.5rem;
  font-size: 1.25rem;
  margin: 0.5rem;


  transition: background-color .1s linear;

  background-color: #e3e5f3;


  &:focus {
    background-color: #d8d9e8;
  }

  &::placeholder {
    color: black;
  }
`;

export const EmailInput = styled.input`

  outline: none;
  border: none;

  padding: 0.5rem;
  margin: 0.5rem;

  font-size: 1.25rem;

  transition: background-color .1s linear;

  background-color: #e3e5f3;


  &:focus {
    background-color: #d8d9e8;
  }

  &::placeholder {
    color: black;
  }
`;

export const PasswordInput = styled.input`

  outline: none;
  border: none;

  padding: 0.5rem;
  margin: 0.5rem;

  font-size: 1.25rem;

  transition: background-color .1s linear;

  background-color: #e3e5f3;


  &:focus {
    background-color: #d8d9e8;
  }

  &::placeholder {
    color: black;
  }
`;

export const OrLine = styled.hr<{text: string}>`
  width: 100%;

  position: relative;

  overflow: visible;

  &:before {
    position: absolute;
    left: 50%;

    padding: 0 0.75rem;

    z-index: 5;

    background-color: #ECEEF5;

    transform: translateX(-50%) translateY(-60%);

    content: '${({ text }) => text}';
  }
`;
