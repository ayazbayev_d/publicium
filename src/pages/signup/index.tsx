import { NavSection } from '@main/common/sidebar/nav';
import React, { FC } from 'react';
import {
	Sidebar,
	Viewport,
} from '@main/common';
import {
	Container,
	Empty,
	LoginButton,
	ViewportCol,
	ViewportRow,
	UsernameInput,
	EmailInput,
	PasswordInput,
	AuthMode,
	SignupButton,
	LoginText,
	SignupText,
	AuthorizeBlock,
	AuthorizeButton,
	AuthorizeText,
	ConnectWithBlock,
	ConnectWithGit,
	InputContainer,
	OrLine,
} from './blocks';

const SignUp: FC = () => (
	<Viewport>
		<Sidebar navSection={NavSection.Service} />
		<ViewportRow>
			<Empty />
			<ViewportCol>
				<Empty />
				<Container>
					<AuthMode>
						<LoginButton>
							<LoginText>Log In</LoginText>
						</LoginButton>
						<SignupButton>
							<SignupText>Sign Up</SignupText>
						</SignupButton>
					</AuthMode>
					<InputContainer>
						<Empty />
						<UsernameInput placeholder={'Username'} />
						<Empty />
					</InputContainer>
					<InputContainer>
						<Empty />
						<EmailInput placeholder={'Email'} />
						<Empty />
					</InputContainer>
					<InputContainer>
						<Empty />
						<PasswordInput placeholder={'Password'} />
						<Empty />
					</InputContainer>


					<AuthorizeBlock>
						<AuthorizeButton>
							<AuthorizeText>
								Sign Up
							</AuthorizeText>
						</AuthorizeButton>
					</AuthorizeBlock>


					<OrLine text={'Or connect with'} />

					<ConnectWithBlock>
						<ConnectWithGit />
						<ConnectWithGit />
						<ConnectWithGit />
						<ConnectWithGit />
					</ConnectWithBlock>
				</Container>
				<Empty />
			</ViewportCol>
			<Empty />
		</ViewportRow>
	</Viewport>
);

export default SignUp;
