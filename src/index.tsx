import React, { ComponentType } from 'react';
import ReactDom from 'react-dom';

import App from './app';

export default (): JSX.Element => <App />;

export const mount = (Component: ComponentType): void => {
	ReactDom.render(
		<Component />,
		document.getElementById('app'),
	);

	if (module.hot) {
		module.hot.accept('./app', () => {
			ReactDom.render(
				<App />,
				document.getElementById('app'),
			);
		});
	}
};

export const unmount = (): void => {
	const mountRoot = document.getElementById('app');

	if (mountRoot) ReactDom.unmountComponentAtNode(mountRoot);
};

