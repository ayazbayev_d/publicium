import Theme from '@main/config/theme';
import store from '@main/redux/store';
import React, { FC } from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';

import DefaultRouter from './router';

import './app.css';

const App: FC = () => (
	<ThemeProvider theme={Theme}>
		<Provider store={store}>
			<DefaultRouter />
		</Provider>
	</ThemeProvider>
);

export default App;

