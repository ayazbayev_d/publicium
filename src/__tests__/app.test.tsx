import Theme from '@main/config/theme';
import {
  BrowserRouter as Router,
} from 'react-router-dom';

import { mount } from 'enzyme';
import React from 'react';
import { describe, it, expect } from '@jest/globals';
import { ThemeProvider } from 'styled-components';
import App from '../index';

describe('<App />', () => {
  it('App renders OK', () => {
    const component = mount(
      <ThemeProvider theme={Theme}>
        <Router>
          <App />
        </Router>
      </ThemeProvider>,
    );

    expect(component).toMatchSnapshot();
  });
});

