
import React from 'react';
import { mount } from 'enzyme';

import { describe, it, expect } from '@jest/globals';
import Search from '../common/search/index';


describe('<Search />', () => {
  it('Search renders OK', () => {
    const wrapper = mount(
      <Search />,
    );

    expect(wrapper.find('input')).toMatchSnapshot();
  });
});
