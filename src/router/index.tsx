import React, { FC } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Collection from '@main/pages/collection';
import Games from '@main/pages/games';
import Home from '@main/pages/home';
import Login from '@main/pages/login';
import Profile from '@main/pages/profile';
import SignUp from '@main/pages/signup';

import NavConfig from '@main/config/routing';

const DefaultRouter: FC = () => (
	<Router basename={NavConfig.baseUrl}>
		<Switch>
			<Route path={NavConfig.pages.signin} component={Login} />
			<Route path={NavConfig.pages.signup} component={SignUp} />
			<Route path={NavConfig.pages.games} component={Games} />
			<Route path={NavConfig.pages.collection} component={Collection} />
			<Route path={NavConfig.pages.user} component={Profile} />
			<Route path={NavConfig.pages.home} component={Home} />
		</Switch>
	</Router>
);

export default DefaultRouter;
