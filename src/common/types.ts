export interface FetchableList<T> {
	loading: boolean;
	error?: string;
	items: T[];
}

export type ReplaceProp<T, Prop extends keyof T, NewType> = Omit<T, Prop> & {
	[key in Prop]: NewType;
}

export interface Game {
	id: number;
	name: string;
	logo: string;
}

export interface GameMap {
	id: number;
	name: string;
	logo: string;
	game: Game;
}


export interface User {
	id: number;
	name: string;
	avatar: string;
}
