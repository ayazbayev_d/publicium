import styled from 'styled-components';
import Background from '@assets/images/backg.png';

export { default as Sidebar } from './sidebar';

export const Viewport = styled.div`
  width: 100vw;
  height: 100vh;
  
  display: flex;
  flex-direction: row;
  
  align-items: stretch;
  justify-content: stretch;
`;

export const Content = styled.main`
  position: relative;
  flex-grow: 1;
  
  width: calc(100vw - 11.25rem);
  
  display: flex;
  flex-direction: column;
  
  box-sizing: border-box;
  
  align-items: stretch;
  justify-content: flex-start;
  
  &:before {
    position: absolute;
    
    z-index: -100;
    
    width: 100%;
    height: 100%;
    
    opacity: 0.5;
    filter: blur(0.2rem);
    
    content: '';
    background-image: url('${Background}');

    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
  }
`;

