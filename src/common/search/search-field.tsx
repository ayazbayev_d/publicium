import React, { FC } from 'react';
import {
	FieldContainer,
	SearchInput,
	SearchButton,
} from './blocks';

const SearchField: FC = () => (
	<FieldContainer>
		<SearchInput placeholder={'Search'} />
		<SearchButton onClick={() => alert('clickt')} />
	</FieldContainer>
);

export default SearchField;
