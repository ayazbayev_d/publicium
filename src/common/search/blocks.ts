import { SearchIcon } from '@assets/icons';
import styled from 'styled-components';

export const Container = styled.header`
  flex-grow: 0;

  margin: 1.5rem 2.5rem;

  display: flex;
  flex-direction: row;

  align-items: stretch;
  justify-content: space-between;
`;

export const FieldContainer = styled.div`
  position: relative;
  
  display: flex;
  flex-direction: row;

  flex-grow: 1;

  align-items: stretch;

  border-radius: 0.5rem;

`;

export const SearchInput = styled.input`
  flex-grow: 1;

  outline: none;
  border: none;

  padding-left: 1.25rem;

  font-size: 1.25rem;

  border-radius: 0.5rem;

  transition: background-color .1s linear;

  background-color: #e3e5f3;
  

  &:focus {
    background-color: #d8d9e8;
  }

  &::placeholder {
    color: black;
  }
`;

export const SearchButton = styled.button`
  position: relative;
  
  z-index: 12;
  
  margin-left: -2.875rem;
  
  outline: none;
  border: none;

  height: 2.875rem;
  width: 2.875rem;
  
  background-color: transparent;
  background-image: url('${SearchIcon}');
  background-size: 50%;
  background-position: center;
  background-repeat: no-repeat;
  
  transition: background-color .1s linear;

  &:hover {
    background-color: #d8d9e8;
  }
  
  &:active {
    background-color: #cfd1dd;
  }
  
  border-top-right-radius: 0.5rem;
  border-bottom-right-radius: 0.5rem;
`;
