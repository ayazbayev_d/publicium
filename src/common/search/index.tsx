import React, { FC } from 'react';
import { Container } from './blocks';
import SearchField from './search-field';

const Search: FC = () => (
	<Container>
		<SearchField />
	</Container>
);

export default Search;
