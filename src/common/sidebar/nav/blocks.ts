import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const Container = styled.nav`
  display: flex;
  flex-direction: column;

  align-items: center;
  justify-content: space-around;

  flex-grow: 5;
`;

export const NavLink = styled(Link)<{ active?: boolean }>`
  text-decoration: none;

  & img {
    height: 3rem;
    width: auto;

    opacity: ${({ active }) => (active ? 1 : 0.6)};
    transition: opacity 0.1s linear;

    &:hover {
      opacity: 0.95;
    }
  }
`;
