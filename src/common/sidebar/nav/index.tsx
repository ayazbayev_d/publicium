import {
	AppsIcon,
	HomeIcon,
	JoyIcon,
} from '@assets/icons';
import React, { FC } from 'react';
import {
	Container,
	NavLink,
} from './blocks';

export enum NavSection {
	Home,
	Games,
	Collection,
	Service
}

interface NavProps {
	section: NavSection;
}

const Nav: FC<NavProps> = ({ section }) => (
	<Container>
		<NavLink
			to={'/'}
			active={section === NavSection.Home}
		>
			<img src={HomeIcon} alt={'Home'} />
		</NavLink>
		<NavLink
			to={'/games'}
			active={section === NavSection.Games}
		>
			<img src={JoyIcon} alt={'Games'} />
		</NavLink>
		<NavLink
			to={'/user/1/collection'}
			active={section === NavSection.Collection}
		>
			<img src={AppsIcon} alt={'Collection'} />
		</NavLink>
	</Container>
);

export default Nav;
