import styled from 'styled-components';

export const Container = styled.div`
  flex-grow: 0;
  
  display: flex;
  flex-direction: column;
  
  align-items: center;
`;

export const Avatar = styled.img`
  height: 5rem;
  width: 5rem;
  
  border-radius: 2.5rem;
`;
