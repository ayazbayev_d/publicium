import React, { FC } from 'react';
import AnonymousAvatar from '@assets/images/person.png';
import { Link } from 'react-router-dom';
import { Container, Avatar } from './blocks';

const UserMenu: FC = () => (
	<Container>
		<Link to={'/user/1'}>
			<Avatar src={AnonymousAvatar} />
		</Link>
	</Container>
);

export default UserMenu;
