import Nav, { NavSection } from '@main/common/sidebar/nav';
import UserMenu from '@main/common/sidebar/user-menu';
import React, { FC } from 'react';
import {
	Container,
	Logo,
	Spacer,
} from './blocks';

interface SidebarProps {
	navSection: NavSection;
}

const Sidebar: FC<SidebarProps> = ({ navSection }) => (
	<Container>
		<Logo />
		<Spacer span={3} />
		<Nav section={navSection} />
		<Spacer span={3} />
		<UserMenu />
	</Container>
);

export default Sidebar;
