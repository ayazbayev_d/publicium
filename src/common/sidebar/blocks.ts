import styled from 'styled-components';

export const Container = styled.aside`
  width: 11.25rem;

  flex-grow: 0;

  background-color: ${({ theme }) => theme.colors.primary};

  padding: 2.25rem 0;
  
  display: flex;
  flex-direction: column;

  align-items: stretch;
  justify-content: space-between;
`;

export const Logo = styled.div`
  flex-grow: 1;
  background-image: url("${({ theme }) => theme.logo}");

  background-position: center;
  background-size: contain;
  background-repeat: no-repeat;
`;

export const Spacer = styled.div<{ span?: number }>`
  flex-grow: ${({ span }) => (span || 1)};
`;

