declare module '@ijl/cli' {
	export function getConfigValue(id: string): string | undefined;

	interface Navigations {
		[key: string]: string | undefined;
	}

	export function getNavigations(id: string): Navigations;
}
