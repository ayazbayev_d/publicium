import GamesApi from '@main/api/games-api';
import {
	apiUrl,
	clapf,
} from '@main/api/util';
import {
	Game,
	GameMap,
	ReplaceProp,
	User,
} from '@main/common/types';
import axios from 'axios';

type ApiMap = ReplaceProp<GameMap, 'game', number>;

class HomeApiProvider {
	fetchTrendingGames(): Promise<Game[]> {
		return axios.get<Game[]>(clapf(apiUrl('/games/trending'), 'rq to url: \'%s\''))
			.then(v => v.data);
	}

	fetchTopUsers(): Promise<User[]> {
		return axios.get<User[]>(apiUrl('/users/top-load'))
			.then(v => v.data);
	}

	fetchTopMaps(): Promise<GameMap[]> {
		return axios.get<ApiMap[]>(apiUrl('/maps/trending'))
			.then(v => v.data)
			.then(maps => Promise.all(
				maps.map(async ({ game, ...map }) => ({
					...map,
					game: await GamesApi.fetchGameById(game),
				})),
			))
			.catch(e => {
				console.error(e);
				throw e;
			})
	}
}

const HomeApi = new HomeApiProvider();

export default HomeApi;
