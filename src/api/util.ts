import ApiConfig from '@main/config/api';

export const clapf = <T>(value: T, format: string): T => {
	console.log(format, value);
	return value;
};

export const clap = <T>(value: T): T => clapf(value, 'Through value: %O');

const cleanupUrlAddr = (addr: string) => clap(addr)
	.replace(/\/+/g, '/');

export const cleanupUrl = (url: string): string => {
	const [proto, addr] = url.split('://');

	if (!addr) {
		return clap(cleanupUrlAddr(proto));
	}

	return [
		proto,
		cleanupUrlAddr(addr),
	].join('://');
};

export const withBaseUrl = (url: string, base?: string): string => (
	base !== undefined
		? cleanupUrl(`${base}/${url}`)
		: url
);

export const apiUrl = (url: string): string => withBaseUrl(url, ApiConfig.Base);
