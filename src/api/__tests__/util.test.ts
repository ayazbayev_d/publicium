import { describe, it, expect } from '@jest/globals';
import { cleanupUrl, withBaseUrl } from '../util';

describe('cleanupUrl', () => {
  it('works for relative urls', () => {
    expect(
      cleanupUrl('/api///games/trending'),
    ).toBe('/api/games/trending');

    expect(
      cleanupUrl('/api/user/1'),
    ).toBe('/api/user/1');
  });

  it('works for absolute urls', () => {
    expect(
      cleanupUrl('http://example.org/api///games/trending'),
    ).toBe('http://example.org/api/games/trending');

    expect(
      cleanupUrl('http://example.org/api/user/1'),
    ).toBe('http://example.org/api/user/1');
  });
});

describe('withBaseUrl', () => {
  it('works with base url', () => {
    expect(
      withBaseUrl('/api/games', 'http://example.org/'),
    ).toBe('http://example.org/api/games');

    expect(
      withBaseUrl('/api/games', '/'),
    ).toBe('/api/games');

    expect(
      withBaseUrl('/api/games', '/api'),
    ).toBe('/api/api/games');
  });

  it('works without base url', () => {
    expect(
      withBaseUrl('/api/games'),
    ).toBe('/api/games');

    expect(
      withBaseUrl('/api/games', undefined),
    ).toBe('/api/games');
  });
});
