import { apiUrl } from '@main/api/util';
import { Game } from '@main/common/types';
import axios from 'axios';

class GamesApiProvider {
	fetchGameById(id: number): Promise<Game> {
		return axios.get<Game>(apiUrl(`/games/${id}`))
			.then(v => v.data);
	}
}

const GamesApi = new GamesApiProvider();

export default GamesApi;
