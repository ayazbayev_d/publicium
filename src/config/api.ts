import { getConfigValue } from '@ijl/cli';

const ApiConfig = {
	Base: getConfigValue('publicium.apiRoot'),
	Headers: {},
};

export default ApiConfig;
