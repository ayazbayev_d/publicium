import { getNavigations } from '@ijl/cli';

const nav = getNavigations('publicium');

const NavConfig = {
	baseUrl: nav['publicium.nav.root'],
	pages: {
		home: nav['publicium.nav.home'],
		signin: nav['publicium.nav.signin'],
		signup: nav['publicium.nav.signup'],
		games: nav['publicium.nav.games'],
		collection: nav['publicium.nav.collection'],
		user: nav['publicium.nav.user'],
	},
};

export default NavConfig;
