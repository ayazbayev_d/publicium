import { DefaultTheme } from 'styled-components';

export const Theme: DefaultTheme = {
	logo: 'https://dummyimage.com/320x200/031E44/ffffff.png&text=Logo',
	colors: {
		primary: '#031E44',
	},
};

export default Theme;
